// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';
console.log('Started');
const {WebhookClient} = require('dialogflow-fulfillment');
console.log('Step 1' );
const {Card, Suggestion} = require('dialogflow-fulfillment');
const BIGQUERY = require("@google-cloud/bigquery");
const { Payload } = require("dialogflow-fulfillment");
const {Carousel, DialogflowConversation, SimpleResponse} = require('actions-on-google');

const functions = require('firebase-functions');
var admin = require("firebase-admin");
console.log('Dialogflow Request headers: Here' );
admin.initializeApp(functions.config().firebase);
console.log('Dialogflow Request headers: Not here' );

//project name
const BIGQUERY_CLIENT = new BIGQUERY({
projectId: "da-bot-proj"
});

var customer_table = 'da-bot-proj.Customer_data.Cust_srv_dlr_details';
var tracking_table = 'da-bot-proj.Customer_data.Customer_res_table';

var Mob_Num;
var mob_num;
var cnt;
var name;
var dealer;
var existing_customer = false;
var date;
var date_pref;
var Dealer_district;
 
process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements
 
exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
const agent = new WebhookClient({ request, response });
//console.log('Dialogflow Request headers: Here is the error' );
//console.log('Dialogflow Request headers start: ' + JSON.stringify(request.headers));
//console.log('Dialogflow Request body start: ' + JSON.stringify(request.body));
console.log('Reaches here' );
 
  function fallback(agent) {
    console.log('Reaches here also');
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
  }
  
  function auth_mobile(agent){
    console.log('Mobile auth');
    agent.add(`Please Enter your 10-digit mobile number.`);
    //agent.setFollowupEvent("Auth_mob_no - custom");
  }
  
  
  function mobnum(agent) {
    //1
    //agent.add(`Reached inside`);
    
    //2
    console.log('Mobile verification Entered');
    console.log('Dialogflow Request headers mobnum: ' + JSON.stringify(request.headers));
    console.log('Dialogflow Request body mobnum: ' + JSON.stringify(request.body));
    
    //3
    let Mob_Num = agent.query;
    console.log('Mobile is getting logged ' + Mob_Num);
    
    
    //4
    const SQLQUERY1 = 'SELECT COUNT(*) AS cnt FROM `' + customer_table + '` WHERE contact_no = @Mob_Num';//project name + table name
    const Dealer_Query = 'SELECT dealership_nm FROM `' + customer_table + '` WHERE contact_no = @Mob_Num';//project name + table name
    
    
    const Num_Ver = {
      query : SQLQUERY1,
      location: "US",
      params: {
        Mob_Num: String(Mob_Num)
      }
    };
    const Deal = {
      query : Dealer_Query,
      location: "US",
      params: {
        Mob_Num: String(Mob_Num)
      }
    };
    return BIGQUERY_CLIENT.query(Num_Ver)
      .then(results => {
      const QUERY_RESULT = results[0];

      console.log(QUERY_RESULT);

      cnt = QUERY_RESULT[0].cnt;
      
      if (cnt>0){
        //agent.add("We found you!");
        existing_customer = true;
       
        //agent.setFollowupEvent({"name":"dealer_confirmation", "parameters":{"Mobile": Mob_Num} });
      }else{existing_customer = false;}
      
      if (existing_customer){
        return BIGQUERY_CLIENT.query(Deal)
        .then(deal_results => {
          
          const DEALER_QUERY = deal_results[0];
          console.log(DEALER_QUERY);
          
          dealer = DEALER_QUERY[0].dealership_nm;
          
          console.log('Dealer id' + dealer);
          mob_num = Mob_Num;
          console.log('Mobile is getting logged ' + mob_num);
          
          agent.setFollowupEvent({"name":"found_dealer", "parameters":{"dealer": dealer}
                                 });
        });
      
      }
      else{
        agent.setFollowupEvent({"name":"new_cust"});
      }
      })//;
      .catch(err => {
        console.error("ERROR:", err);});
  }
  
  
  function mobnum_change_dealer(agent){
    
    console.log("Changing DEALER");
    
    console.log('Dialogflow Request headers CHANGED DEALER: ' + JSON.stringify(request.headers));
    console.log('Dialogflow Request body CHANGED DEALER: ' + JSON.stringify(request.body));
    
    //console.log("reaching beyond the variables "+request.body.queryResult.outputContexts[0].parameters.mob_num);
    Mob_Num = request.body.queryResult.outputContexts[0].parameters.mob_num;
    
    const query_city_code = 'SELECT dealer_district AS code FROM `' + customer_table + '` WHERE contact_no = @Mob_Num';
    
    console.log("reading mobile number" + Mob_Num);
    console.log("reaching beyond the variables");
    
    const city_code = {
      query : query_city_code,
      location: "US",
      params: {
        Mob_Num: String(Mob_Num)
      }
    };
        
    return BIGQUERY_CLIENT.query(city_code)
      .then(results => {
      console.log("logs " + JSON.stringify(results[0]));
      
      const QUERY_RESULT = results[0];
      
      Dealer_district = QUERY_RESULT[0].code;
      
      console.log("Query result and result "+ Dealer_district);
      
      if(Dealer_district != undefined){
        console.log('Dialogflow Request body CHANGED DEALER 1: ' + JSON.stringify(request.body));
        agent.setFollowupEvent({"name":"Dist_data", "parameters":{'district_name':Dealer_district}});
      }
      else
      {
        console.log("Not Found");
      }
      
    });
  }
  
  
  function table_of_dealers(agent){
    
    console.log('Dialogflow Request body CHANGED TABLE: ' + JSON.stringify(request.body));
    console.log('CHANGED TABLE: ' + request.body.queryResult.outputContexts[0].parameters.district_name);
    
    Dealer_district = request.body.queryResult.outputContexts[0].parameters.district_name;
    
    var qstr1 = 'SELECT dealership_nm, dealer_pincode, COUNT(*) AS `num` FROM `';
    var qstr2 = '` WHERE dealer_district = @dealer_district GROUP BY dealership_nm, dealer_pincode ORDER BY num DESC LIMIT 5';
    //const dealers_query = qstr1 + customer_table + qstr2;
    const dealers_query = 'SELECT dealership_nm, dealer_pincode, COUNT(*) AS `num` FROM `'+ customer_table + '` WHERE dealer_district = @dealer_district GROUP BY dealership_nm, dealer_pincode ORDER BY num DESC LIMIT 5';
    
    const dealer_table = {
      query : dealers_query,
      location: "US",
      params: {
        dealer_district: Dealer_district
      }
    };
    return  BIGQUERY_CLIENT.query(dealer_table)
      .then(results => {
      
      const QUERY_RESULT = results[0];
                  
      console.log("logs TABLE " + JSON.stringify(results[0]));
      
      agent.setFollowupEvent({"name":"table_dealers", "parameters":{"dealer1":QUERY_RESULT[0].dealership_nm, "dealer1_pin":QUERY_RESULT[0].dealer_pincode,
                                                                    "dealer2":QUERY_RESULT[1].dealership_nm, "dealer2_pin":QUERY_RESULT[1].dealer_pincode,
                                                                    "dealer3":QUERY_RESULT[2].dealership_nm, "dealer3_pin":QUERY_RESULT[2].dealer_pincode}});
      
    });
    
  }
  
  
  function dates(agent){
    console.log('Dialogflow Request headers date: ' + JSON.stringify(request.headers));
	console.log('Dialogflow Request body date: ' + JSON.stringify(request.body));
    
    console.log("Logging date here: " + mob_num);
    
    //var OUTPUT_CONTEXTS = agent.getContext('found_dealer').parameters['dealer'];
    //console.log("Output" + OUTPUT_CONTEXTS);
    
    //agent.add("Reached date selection");
    
    const today = new Date();
    const tomorrow = new Date();
    
    const d1 = new Date(tomorrow.setDate(today.getDate() + 1));
    const d2 = new Date(tomorrow.setDate(today.getDate() + 2));
    const d3 = new Date(tomorrow.setDate(today.getDate() + 3));
    const d4 = new Date(tomorrow.setDate(today.getDate() + 4));


    console.log('Todays date :' + today.toDateString()+d1.toDateString()+d2.toDateString());
    //console.log('Tomorrows date :' + tomorrow.toDateString());
    
    var todays_date = today.toDateString();
    var tomorrows_date = d1.toDateString();
    var todays_2 = d2.toDateString();
    var todays_3 = d3.toDateString();
    var todays_4 = d4.toDateString();
    
    agent.setFollowupEvent({"name":"date_selection", "parameters":{"today":todays_date, "tomorrow":tomorrows_date, 
                                                                   "tomorrow_1":todays_2, "tomorrow_2":todays_3, "tomorrow_3":todays_4}
                                 });
    
  }
  
  function receive_date_selection(agent){
    
    
    console.log('Dialogflow Request headers REC DATES: ' + JSON.stringify(request.headers));
	console.log('Dialogflow Request body REC DATES: ' + JSON.stringify(request.body));
    
    const OUTPUT_CONTEXTS = request.body.queryResult.parameters.date;
    //date = OUTPUT_CONTEXTS[0].parameters.date;
    
    //console.log("date " + date);
    console.log("OUTPUT_CONTEXTS " + OUTPUT_CONTEXTS);
    
   agent.setFollowupEvent({"name":"date_test", "parameters":{"date" : OUTPUT_CONTEXTS}}); 
    
  }
  
  function receive_time_selection(agent){
    
    console.log('Dialogflow Request headers REC TIME: ' + JSON.stringify(request.headers));
	console.log('Dialogflow Request body REC TIME: ' + JSON.stringify(request.body));
    
    agent.setFollowupEvent({"name":"end"});
    
  }
  
  function customdate(agent){
    
    console.log('Dialogflow Request headers customdate: ' + JSON.stringify(request.headers));
	console.log('Dialogflow Request body customdate: ' + JSON.stringify(request.body));
    
    agent.add(`Please enter preferred date.`);
    
    let date = agent.query;
    
    agent.setFollowupEvent({"name":"confirm_date", "parameters":{"date" : date}
                                 });
  
  
  }
  
  function finalization(agent){
    
    console.log('Dialogflow Request headers finalization: ' + JSON.stringify(request.headers));
	console.log('Dialogflow Request body finalization: ' + JSON.stringify(request.body));
    
    console.log("Confirmation Step:" + mob_num);
    
    //mob_num = mob_num;
    dealer = request.body.queryResult.outputContexts[1].parameters.dealer;
    
    console.log("Confirmation Step:" + dealer);
    
    console.log("Confirmation Step:" + date);
    
    //agent.add(`Your mobile number is ` + mob_num);
    //agent.add(`Your selected dealer is `+ dealer);//+ agent.parameters.dealer);
    //agent.add(`Your selected date is 30th Nov 2021`);//+ agent.parameters.date_pref);
    //agent.add(`Your selected timeslot is 9AM - 12PM`);//+ time)
    
    agent.setFollowupEvent({"name":"details"});
    
    //const OUTPUT_CONTEXTS = request.body.queryResult.outputContexts;
    //console.log("OUTPUT_CONTEXTS " + OUTPUT_CONTEXTS[OUTPUT_CONTEXTS.length-2].parameters.dealer);
    //console.log("OUTPUT_CONTEXTS " + OUTPUT_CONTEXTS[OUTPUT_CONTEXTS.length-1].parameters.dealer);
    //console.log("OUTPUT_CONTEXTS " + OUTPUT_CONTEXTS[OUTPUT_CONTEXTS.length].parameters.dealer);
    //console.log("OUTPUT_CONTEXTS " + agent.call(dealer));
  }


  // // Uncomment and edit to make your own intent handler
  // // uncomment `intentMap.set('your intent name here', yourFunctionHandler);`
  // // below to get this function to be run when a Dialogflow intent is matched
  // function yourFunctionHandler(agent) {
  //   agent.add(`This message is from Dialogflow's Cloud Functions for Firebase editor!`);
  //   agent.add(new Card({
  //       title: `Title: this is a card title`,
  //       imageUrl: 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png',
  //       text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
  //       buttonText: 'This is a button',
  //       buttonUrl: 'https://assistant.google.com/'
  //     })
  //   );
  //   agent.add(new Suggestion(`Quick Reply`));
  //   agent.add(new Suggestion(`Suggestion`));
  //   agent.setContext({ name: 'weather', lifespan: 2, parameters: { city: 'Rome' }});
  // }

  // // Uncomment and edit to make your own Google Assistant intent handler
  // // uncomment `intentMap.set('your intent name here', googleAssistantHandler);`
  // // below to get this function to be run when a Dialogflow intent is matched
  // function googleAssistantHandler(agent) {
  //   let conv = agent.conv(); // Get Actions on Google library conv instance
  //   conv.ask('Hello from the Actions on Google client library!') // Use Actions on Google library
  //   agent.add(conv); // Add Actions on Google library responses to your agent's response
  // }
  // // See https://github.com/dialogflow/fulfillment-actions-library-nodejs
  // // for a complete Dialogflow fulfillment library Actions on Google client library v2 integration sample

  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();

  intentMap.set('1_Default Fallback Intent', fallback);
  intentMap.set('2_Auth_mob_no', auth_mobile);
  intentMap.set('3_Auth_mob_no - custom', mobnum);
  intentMap.set('5_Select_date', dates);
  intentMap.set('Custome_select_date', customdate);
  intentMap.set('final', finalization); 
  intentMap.set('test_intent', receive_date_selection);
  intentMap.set('time_intent', receive_time_selection);
  intentMap.set('Change_Dealer_same_city_int', mobnum_change_dealer); 
  intentMap.set('Change_Dealer_table', table_of_dealers); 
  
  // intentMap.set('your intent name here', yourFunctionHandler); 
  // intentMap.set('your intent name here', googleAssistantHandler);
  agent.handleRequest(intentMap);
});

